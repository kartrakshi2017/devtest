package Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import Pages.LoginPage;

public class RowIterator {
	
	static String Path_TestData = "C:\\Users\\Probe7\\Desktop\\TestData.xlsx";
	
	public void Iterations() throws IOException {
		
		FileInputStream file = new FileInputStream(Path_TestData);
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
		for(Row row:sheet) {
			for(Cell cell:row) {
				System.out.print(cell.getStringCellValue()+"\t");
				
			}
			
		}
		
	}

}
