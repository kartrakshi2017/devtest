package Utility;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GetAttribute {
	
	static WebDriver d;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver","H:\\Karthik\\New soft\\chromedriver_win32\\chromedriver.exe");
	 	d = new ChromeDriver();
        d.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        d.get("http://www.vrlbus.in/vrl2013/register_customer.aspx");
        d.findElement(By.id("FIRSTNAME")).sendKeys("User143");
        assertEquals("User143",d.findElement(By.id("FIRSTNAME")).getAttribute(null));
	}

}
