package Utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NameValidation {
	public static WebDriver d;
	
	public static void main(String[] args) {
		
		 
		 	System.setProperty("webdriver.chrome.driver","H:\\Karthik\\New soft\\chromedriver_win32\\chromedriver.exe");
		 	d = new ChromeDriver();
	        d.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	        d.get("http://www.vrlbus.in/vrl2013/register_customer.aspx");
	        d.findElement(By.id("FIRSTNAME")).sendKeys("$^$^$$$^");
	        d.findElement(By.id("Button1")).click();
	        String alertMessage = d.switchTo().alert().getText();
	        System.out.println(alertMessage);
	      if (alertMessage.equals("First name Should not contain Special Characters")){
	            System.out.println("Error displayed: First name Should not contain Special Characters");
	            d.switchTo().alert().dismiss();
	        } else{
	            System.out.println("Accepted");
	        }
//	        d.findElement(By.id("FIRSTNAME")).sendKeys("acbcdefghijklmnopqrstuvwxyzabcdef");
//	        d.findElement(By.id("Button1")).click();
//	         if (alertMessage.equals("First name Should not contain Special Characters")){
//	                System.out.println("Error displayed: First name Should not contain Special Characters");
//	                d.switchTo().alert().dismiss();
//	            } else{
//	                System.out.println("Accepted");
//	            }
////	        d.quit();
	

	}

}


===============================================================================================================================================================================================================================

//Class Testdiary_VerifyTitle
public class Testdiary_VerifyTitle {

//Main method
public static void main(String[] args) {

//Open a new Firefox Browser
WebDriver driver = new FirefoxDriver();

//Maximise the browser window
driver.manage().window().maximize();

//Save the url of the page into a string object
String appUrl = "http://www.testdiary.com/";

//Open the application url
driver.get(appUrl);

//Declare and initialise the variable to store the expected title of
//the web page.
String expectedTitle = "Test Diary - A software testers guide";

//Fetch the title of the web page and save it into a string variable
String actualTitle = driver.getTitle();

//Use an if condition to check if both string objects are equal
if (expectedTitle.equals(actualTitle)) {

System.out
.println("verification successful - The correct Tile is Displayed on the Webpage");
} else {
System.out.println("verification unsuccessful");
}

//Close the browser
driver.close();

//End the program
System.exit(0);
}
}
==============================================================================================================================================================================================================================


public class Testdiary_ContactPage {

public static void main(String[] args) throws InterruptedException {

// Open a new firefox browser
WebDriver driver = new FirefoxDriver();

driver.manage().window().maximize();

// Initialise the variable to store the URL
String contactUrl = "http://www.testdiary.com/training/selenium/selenium-test-page/";

// Navigate to the URL
driver.get(contactUrl);

// Allow the current thread (the process) to sleep for some seconds.
// Gives The page time to load.
Thread.sleep(4000);

// locate the Name text box on the webpage. Using the locator "By.name"
// Store this text box location to the variable "name"
WebElement name = driver.findElement(By.name("your-name"));

// clear the text box
name.clear();

// Insert your name into the text box using the sendKeys method.
// I am using my name in this scenario
// You can input your own name
name.sendKeys("Femi");

// locate the email text box. Using the locator By.name
// store this text box location to the variable "email"
WebElement email = driver.findElement(By.name("your-email"));

// clear the text box
email.clear();
==============================================================================================================================================================================================================================

Validation in selenium means checking wether all the UI elements are rendered on the webpage or not.

common use of Validation can be for checking whether the hidden elements are actually hidden or not.

Validation also used to check that all the forms are working or not, all the input field is editable or not.

The types of validation that are generally done are as follows:

Page Title Validation
Page URL Validation
Scroll Down
Verifying hidden elements
Checkbox Validations
Radio button Validations
To verify whether a dropdown allows multiple selections or not.
Read-only property of WebElement.
Check if an Element exists or not.
Textbox value validation
Disabled/Enabled property of a Web Element
Page Content Validations

================================================================================================================================================================================================================================

public static void main(String[] args) throws Exception {
    String[] invalidChars = {"#", "!", "$", "@", "%", "^", "&"};
    String name = "acbcdefghijklmnopqrstuvwxyzab";
    d = new FirefoxDriver();
    d.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    d.get("http://www.vrlbus.in/vrl2013/register_customer.aspx");
    for (String invalid : invalidChars) {
        d.findElement(By.id("FIRSTNAME")).clear();
        d.findElement(By.id("FIRSTNAME")).sendKeys(name + invalid);
        d.findElement(By.id("Button1")).click();
        String alertMessage = d.switchTo().alert().getText();
        System.out.println(invalid);
        if (alertMessage.equals("First name Should not contain Special Characters")) {
            System.out.println("Error displayed: First name Should not contain Special Characters");
            d.switchTo().alert().dismiss();
        } else {
            System.out.println("Accepted");
        }
    }
    d.findElement(By.id("FIRSTNAME")).sendKeys("acbcdefghijklmnopqrstuvwxyzabcdef");
    d.findElement(By.id("Button1")).click();
    String alertMessage = d.switchTo().alert().getText();
    if (alertMessage.equals("First name Should not contain Special Characters")) {
        System.out.println("Error displayed: First name Should not contain Special Characters");
        d.switchTo().alert().dismiss();
    } else {
        System.out.println("Accepted");
    }
    d.quit();
}

======================================================================================================================================================================================================================================