package Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CurrentDate {

	public static void main(String[] args) {
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		//get current date time of system with Date()
		
		Date date = new Date();
		
		String date1= dateFormat.format(date);  // Now format the date
		
		// Print the Date
		System.out.println("Current date and time is " +date1);

	}

}
